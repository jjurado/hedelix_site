<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle xs-only" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav .font-philosopher">
            <li><a href="sobre.php" class="font-philosopherc"><b>Sobre Garmisch Hedelix</b></a></li>
            <li><span class="slash-bar font-philosopher">/</span></li>
            <li class="active"><a href="puntos-ventas.php" class="font-philosopher"><b>Puntos de venta</b></a></li>
        </ul>
    </div>
</nav>
    <div class="content">
        <div class="content-container">
        	<div id="map" style="height: 300px"></div>
        	<h2 class="text-center">Puntos de venta principales:</h2>
        	<div class="puntos-ventas-logos">
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/pidefarma.png" class="img-responsive">
        		</div>
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/colsubsidio.png" class="img-responsive">
        		</div>
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/exito.png" class="img-responsive">
        		</div>
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/exito.png" class="img-responsive">
        		</div>
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/colsubsidio.png" class="img-responsive">
        		</div>
        		<div class="puntos-imgs">
        			<img src="imgs/content-puntos-template/exito.png" class="img-responsive">
        		</div>
        	</div>
        	<div class="puntos-ventas">
        		<div class="ciudades">
                    <div class="direcciones">
                        <h4><b><i>Bogotá</i></b></h4>
            			<ul>
            				<li>
            					<b>Calle 52 # 13-70</b><br>
            					Éxito Calle 53
            				</li>
            				<li>
            					<b>Carrera 7 # 32-84</b><br>
            					Éxito San Martín
            				</li>
            				<li>
                                <b>Calle 35A # 73-02</b><br>
            					Éxito Esperanza
            				</li>
                            <li>
                                <b>Calle 26 # 12A-48</b><br>
            					Éxito Gran Estación
            				</li>
            			</ul>
                    </div>
        		</div>
        		<div class="ciudades">
                    <div class="direcciones">
                        <h4><b><i>Cali</i></b></h4>
            			<ul>
            				<li>
            					<b>Calle 52 # 13-70</b><br>
            					Éxito Calle 53
            				</li>
                            <li>
                                <b>Carrera 7 # 32-84</b><br>
            					Éxito San Martín
            				</li>
            				<li>
                                <b>Calle 35A # 73-02</b><br>
            					Éxito Esperanza
            				</li>
            				<li>
                                <b>Calle 26 # 12A-48</b><br>
            					Éxito Gran Estación
            				</li>
            			</ul>
                    </div>
        		</div>
        		<div class="ciudades">
                    <div class="direcciones">
                        <h4><b><i>Medellín</i></b></h4>
            			<ul>
            				<li>
            					<b>Calle 52 # 13-70</b><br>
            					Éxito Calle 53
            				</li>
                            <li>
                                <b>Carrera 7 # 32-84</b><br>
            					Éxito San Martín
            				</li>
            				<li>
                                <b>Calle 35A # 73-02</b><br>
            					Éxito Esperanza
            				</li>
            				<li>
            					<b>Calle 26 # 12A-48</b><br>
            					Éxito Gran Estación
            				</li>
            			</ul>
                    </div>
        		</div>
                <div class="ciudades">
                    <div class="direcciones">
                        <h4><b><i>Pereira</i></b></h4>
            			<ul>
            				<li>
            					<b>Calle 52 # 13-70</b><br>
            					Éxito Calle 53
            				</li>
                            <li>
                                <b>Carrera 7 # 32-84</b><br>
                                Éxito San Martín
            				</li>
            				<li>
                                <b>Calle 35A # 73-02</b><br>
            					Éxito Esperanza
            				</li>
            				<li>
                                <b>Calle 26 # 12A-48</b><br>
            					Éxito Gran Estación
            				</li>
            			</ul>
                    </div>
        		</div>
        		<div class="ciudades">
                    <div class="direcciones">
                        <h4><b><i>Barranquilla</i></b></h4>
            			<ul>
            				<li>
            					<b>Calle 52 # 13-70</b><br>
            					Éxito Calle 53
            				</li>
            				<li>
                                <b>Carrera 7 # 32-84</b><br>
            					Éxito San Martín
            				</li>
                            <li>
            					<b>Calle 35A # 73-02</b><br>
            					Éxito Esperanza
            				</li>
            				<li>
                                <b>Calle 26 # 12A-48</b><br>
            					Éxito Gran Estación
            				</li>
            			</ul>
                    </div>
        		</div>
        	</div>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	    <script>

	    $( document ).ready(function(){
	    	console.log('Iniciado');
	    	initMap()
	    });
	var map;
	function initMap() {
		console.log('Init Map')
	  map = new google.maps.Map(document.getElementById('map'), {
	    center: {lat: -34.397, lng: 150.644},
	    zoom: 8
	  });
	}

	    </script>
        </div>
    </div>
</div>
