<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Cabin|Philosopher" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Hedelix</title>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
</head>
<body>
    <div class="container-fluid">
    <header>
            <div class="logo-header">
                <a href="index.php"><img src="imgs/header-template/logo.png" class="img-responsive center-block" alt="Logo Hedelix"></a>
            </div>
            <div class="logo-text">
                <h1 class="text-center blue-hedelix"><b>Hedera Helix</b></h1>
            </div>
            <div class="sub-logo-text">
                <h3 class="text-center blue-hedelix"><b>Expectorante sin azúcar de origen natural.</b></h3>
            </div>
    </header>
