<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle xs-only" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav .font-philosopher">
            <li><a href="sobre.php" class="font-philosopher"><b>Sobre Garmisch Hedelix</b></a></li>
            <li><span class="slash-bar font-philosopher">/</span></li>
            <li><a href="puntos-ventas.php" class="font-philosopher"><b>Puntos de venta</b></a></li>
        </ul>
    </div>
</nav>
    <div class="col-lg-4 col-lg-offset-1 content-container-home">
        <div class="logo-content">
            <img class="img-responsive center-block" src="imgs/content-home-template/logo-banner2.png" alt="">
        </div>
        <div class="logo-content-btn">
            <button class="btn btn-primary center-block content-btn font-philosopher" type="button" name="button" value=""><b><i>¡Cómpralo ya!</i></b></button>
        </div>
    </div>
</div>
