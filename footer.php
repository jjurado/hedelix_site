<footer>
    <div class="container">
        <div class="row publicy">
            <div class="col-xs-12 col-sm-4">
                <h3 class="text-center"><b>Publicidad válida sólo en Colombia</b></h3>
            </div>
            <div class="col-xs-6 col-sm-4">
                <img class="img-responsive logo-garmisch"  src="imgs/footer-template/logo-footer.png" alt="Garmisch Pharmaceutical">
            </div>
            <div class="col-xs-6 col-sm-4">
                <img class="img-responsive logo-voreau"  src="imgs/footer-template/iso.png" alt="Voreau Veritas Certification">
            </div>
        </div>
        <div class="row foot-mid">
            <ul class="navbar-nav">
                <li><h4><b>Garmisch Hedelix. Garmisch Pharmaceutical. Colombia. 2015</b></h4></li>
                <li><span>•</span></li>
                <li><a class="blue-hedelix" href="#"><b>Términos y condiciones</b></a></li>
            </ul>
        </div>
        <div class="foot-bot">
            <p class="text-center"><b>Registro sanitario PFM2011-000|723. Es un medicamento fitoterapéutico; no exceder su consumo.</b></p>
            <p class="text-center"><b>Leer indicaciones y contraindicaciones. Si los síntomas persisten, consulte a su médico.</b></p>
        </div>
    </div>
</footer>
</body>
</html>
<link rel="stylesheet" href="index.css">
