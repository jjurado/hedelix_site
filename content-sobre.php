<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle xs-only" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="glyphicon glyphicon-menu-hamburger"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav .font-philosopher">
            <li class="active"><a href="sobre.php" class="font-philosopher"><b>Sobre Garmisch Hedelix</b></a></li>
            <li><span class="slash-bar font-philosopher">/</span></li>
            <li><a href="puntos-ventas.php" class="font-philosopher"><b>Puntos de venta</b></a></li>
        </ul>
    </div>
</nav>
    <div class="content">
        <div class="content-container">
            <div class="text-content">
                <p><b>Garmisch HEDELIX®</b> es un expectorante de origen natural sin azúcar que ayuda a expulsar
                    las flemas y controlar la tos.</p>
            </div>
            <div class="img-content">
                <img class ="img-responsive" src="imgs/content-sobre-template/img-sobre.png" alt="">
            </div>
            <div class="content-ficha border-none">
                <h2 class="text-center">Conoce la ficha técnica</h2>
                <button type="button" class="btn btn-info btn-lg btn-modal" data-toggle="modal" data-target="#myModal">Clic Aquí</button>
                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title">Alpex <span class="blue-hedelix">HEDELIX</span>®</h3>
                          </div>
                          <div class="modal-body">
                            <h4>Composición:</h4>
                            <p>Cada 100 ml contiene 0.7g de extracto seco de Hedera Helix.</p>
                            <h4>Propiedades Farmacológicas:</h4>
                            <p>En las vías respiratorias HEDELIX® ofrece propiedades secretolíticas, espasmolíticas y antitusígenas. Estas propiedades provienen de su mecanismo de acción:<br><br>HEDELIX® favorece la expectoración: Las saponinas del extracto  de Hedera Hélix excitan la mucosa gástrica,  con lo cual activa las fibras parasimpáticas aferentes quienes a su vez, a nivel bronquial, estimulan la secreción de las células caliciformes de la mucosa, aumenta la producción de surfactante y por ende la reducción de la viscosidad del moco. HEDELIX® induce broncodilatación: Los saponósidos producen broncodilatación al inhibir la endocitosis de los receptores beta dos (β2)  en la membrana celular de la fibra muscular lisa del bronquio, con esto logra que el acople de la adrenalina con su receptor se favorezca lo que conlleva a la relajación del músculo bronquial. HEDELIX®, controla la tos: Las propiedades, secretolíticas y relajantes del músculo bronquial, disminuyen la frecuencia e intensidad de la tos.</p>
                            <h4>Indicaciones:</h4>
                            <p>Coadyuvante para el manejo de la tos.</p>
                            <h4>Administración y Posologia:</h4>
                            <p>Niños pequeños: 2.5 ml cada 8 horas (3 veces al día). Niños mayores de 7 años: 5 ml cada 8 horas (3 veces al día). Adultos: 5 a 7,5 ml cada
                            8 horas (3 veces al día). Agitar el frasco antes de usar.</p>
                            <h4>Contraindicaciones y Advertencias:</h4>
                            <p>Reacciones de hipersensibilidad. Embarazo y lactancia.</p>
                            <h4>Efectos Adversos:</h4>
                            <p>Reacciones de hipersensibilidad. Embarazo y lactancia.</p>
                            <h4>Presentación:</h4>
                            <p>Caja plegadiza con frasco de vidrio ámbar con tapa plástica por 100 ml, más copa dosificadora. (Reg. Sanitario  PFM2011-0001723)</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-modal" data-dismiss="modal">Cerrar</button>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
            <div class="content-social border-none">
                <div class="content-social-text font-philosopher">
                    <p><b><i>!Ya lo sabes tú,</br>ahora compártelo!</i></b></p>
                </div>
                <div class="content-social-icons">
                    <a href="#"><img src="imgs/content-sobre-template/share/facebook.png" alt="Facebook"></a>
                    <a href="#"><img src="imgs/content-sobre-template/share/twitter.png" alt="Twitter"></a>
                    <a href="#"><img src="imgs/content-sobre-template/share/link.png" alt="Link"></a>
                </div>
            </div>
        </div>
    </div>
</div>
